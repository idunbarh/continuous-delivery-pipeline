![Hoppr Examples](https://gitlab.com/hoppr/hoppr/-/raw/dev/media/hoppr-repo-banner.png)

# Continuous Delivery Pipeline Example

This is an example of how to setup a continuous delivery pipeline between secure
environments using [Hoppr](https://hoppr.dev/). [Hoppr](https://hoppr.dev/) allows
multiple teams to declare development dependencies or products using CycloneDX Software
Bill of Materials. These are then collected, analyzed, and packaged by Hoppr to create
a single delivery pipeline.

This allows a security team to have an [in-toto](https://in-toto.io/) attested delivery
with Software Bill of Materials and Reporting for what is transferred in a secure environment.

## Key Concepts

- Multiple teams each define "what" they want transferred
  - [sample-python-dependencies](https://gitlab.com/hoppr/examples/continuous-delivery-demo/sample-python-dependencies)
  from a team needing python packages
  - [sample-dev-tools](https://gitlab.com/hoppr/examples/continuous-delivery-demo/sample-dev-tools)
  from a team needing development tooling
- All teams are using [semantic-release](https://github.com/semantic-release/semantic-release) to
  semantic version their Software Bill of Materials to track changes
- All teams are using [renovate](https://github.com/renovatebot/renovate) to detect dependency updates
- The pipeline to collect, analyze, and package dependencies for transfer automatically
- [In-toto](https://in-toto.io/) layout file is generated based on the Hoppr [transfer.yaml](transfer.yml)
- [In-toto](https://in-toto.io/) link/attestations are generated for each stage of Hoppr processing

## Executing Example

- [Run on gitpod to avoid setup!](https://gitpod.io/#https://gitlab.com/hoppr/examples/continuous-delivery-demo/continuous-delivery-pipeline/-/tree/main/)

### Setup (Skip if using Gitpod)

This example is assuming your running in a debian environemnt with __python 3.10 or later__.

Install ruby, grype and trivy.

```bash
apt install curl ruby-full --yes
curl -sSfL https://raw.githubusercontent.com/anchore/grype/main/install.sh | sh -s -- -b /usr/local/bin
curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin v0.31.2
```

Install Hoppr and Hoppr-Cop a Hoppr plugin that pulls CVE data from multiple databases.

```bash
pip install hoppr hoppr-cop
```

### Create In-Toto Layout and Keys

We encourage teams to use attestations to show provenance and integrity of Hoppr deliveries. Hoppr supports
[in-toto](https://in-toto.io/) and allows team to generate in-toto layout files directly from
Hoppr transfer files. The Hoppr transfer file defines the stages and plugins used to create a
Hoppr bundle for delivery.

Lets create a ...

- product owner gpg key
- functionary gpg key
- in-toto `layout` file from Hoppr's transfer.yml

```bash
in-toto-keygen product_owner_key
in-toto-keygen functionary_key
hopctl generate-layout -t transfer.yml -pk product_owner_key -fk functionary_key
```

The `layout` and `product owner public key` should be transferred to the secure environment "out of band"
and NOT transferred with the Hoppr bundle. These are used for independent verification of the Hoppr
bundle.

### Run Hoppr to Collect, Analyze, and Package

Run the `hopctl bundle` command which will use a [transfer.yml](transfer.yml) to process the Software Bill of Materials
listed in a [manifest.yml](manifest.yml).

```bash
hopctl bundle --transfer transfer.yml --log hoppr_log.txt --verbose --attest -fk functionary_key manifest.yml
```

This will produce ...

## Understanding the Hoppr files

### [manifest.yml](manifest.yml)

The [manifest.yml](manifest.yml) file defines "what" and "where" to find dependencies.

- The "what" is defined in CycloneDX Software Bill of Materials (seen in the `sboms` section)
- The "where" is defined in the `repositories` section which allows a preferred order of repositories
to check for each PURL type
- Manifest files can be "nested" by including others (seen in the `includes` section)

```yaml
schemaVersion: v1
kind: manifest

metadata:
  name: Continuous Delivery Pipeline
  version: main
  description: |
    Manifest showing how hoppr manifests can span multiple gitlab projects. This allows
    teams to define their dependencies to be incorporated in larger transfers.

sboms:
  - local: hoppr.cdx.json

includes:
  - url: https://gitlab.com/hoppr/examples/continuous-delivery-demo/sample-dev-tools/-/raw/v1.1.7/manifest.yml?inline=false
  - url: https://gitlab.com/hoppr/examples/continuous-delivery-demo/sample-python-dependencies/-/raw/v1.3.11/manifest.yml?inline=false

repositories:
  gitlab:
    - url: https://gitlab.com
  pypi:
    - url: https://pypi.org
  git:
    - url: https://github.com
```

### [transfer.yml](transfer.yml)

The [transfer.yml](transfer.yml) file defines the order of processing within hoppr and this
[transfer.yml](transfer.yml) consists of 3 `stages` that run serially. The stage names are
arbitrary but are descriptive in this example.

#### Collect

The `Collect` stage downloads all of the components listed in the Software Bill fo Materials listed
in the [manifest.yml] and its nested references. Hoppr uses a plugin architecture to all customization,
but there exist core plugins for common PURL types.

The `Report` stage include the [hoppr-cop](https://gitlab.com/hoppr/hoppr-cop) plugin which pulls
CVE data from 4 databases (Gemnasium, Grype, Trivy, and OSS Index). Other types of plugins can
run here to collect metadata.

The `Bundle` stage includes the `bundle_tar` plugin and creates a `tar.gz` file with all of the bundle
artifacts.

```yaml
---
schemaVersion: v1
kind: transfer

stages:
  Collect:
    plugins:
    - name: hoppr.core_plugins.collect_git_plugin
    - name: hoppr.core_plugins.collect_raw_plugin
    - name: hoppr.core_plugins.collect_pypi_plugin
      config:
        pip_command: pip3
  Report:
    plugins:
    - name: "hopprcop.hoppr_plugin.hopprcop_plugin"
      config:
        scanners: 
          - hopprcop.gemnasium.gemnasium_scanner.GemnasiumScanner
          - hopprcop.grype.grype_scanner.GrypeScanner
          - hopprcop.trivy.trivy_scanner.TrivyScanner
          - hopprcop.ossindex.oss_index_scanner.OSSIndexScanner
  Bundle:
    plugins:
    - name: "hoppr.core_plugins.bundle_tar"
      config:
        tarfile_name: continuous-delivery-pipeline.tar.gz

max_processes: 10
```
